This is gnunet.info, produced by makeinfo version 6.7 from gnunet.texi.

Copyright © 2001-2019 GNUnet e.V.

   Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
copy of the license is included in the section entitled “GNU Free
Documentation License”.

   A copy of the license is also available from the Free Software
Foundation Web site at <http://www.gnu.org/licenses/fdl.html>.

   Alternately, this document is also available under the General Public
License, version 3 or later, as published by the Free Software
Foundation.  A copy of the license is included in the section entitled
“GNU General Public License”.

   A copy of the license is also available from the Free Software
Foundation Web site at <http://www.gnu.org/licenses/gpl.html>.
INFO-DIR-SECTION Networking
START-INFO-DIR-ENTRY
* GNUnet: (gnunet).       Framework for secure peer-to-peer networking
END-INFO-DIR-ENTRY


Indirect:
gnunet.info-1: 1127
gnunet.info-2: 302845
gnunet.info-3: 602561

Tag Table:
(Indirect)
Node: Top1127
Node: Preface4095
Node: About this book4757
Node: Contributing to this book6696
Node: Introduction8114
Node: Project governance12253
Node: Typography13487
Node: Philosophy13926
Node: Design Principles14684
Node: Privacy and Anonymity16058
Node: Practicality17029
Node: Key Concepts17655
Node: Authentication18250
Node: Accounting to Encourage Resource Sharing21203
Node: Confidentiality23433
Node: Anonymity24537
Node: How file-sharing achieves Anonymity26185
Node: Deniability28793
Node: Peer Identities30114
Node: Zones in the GNU Name System (GNS Zones)30802
Node: Egos32499
Node: Installing GNUnet32892
Node: Installing dependencies33959
Node: Getting the Source Code38026
Node: Create user and groups for the system services38697
Node: Preparing and Compiling the Source Code40256
Node: Installation42151
Node: NSS plugin (Optional)42731
Node: Installing the GNS Certificate Authority (Optional)44162
Node: Minimal configuration45112
Node: Checking the Installation45938
Node: Starting GNUnet46631
Node: gnunet-gtk48531
Node: Statistics49313
Node: Peer Information50746
Node: The graphical configuration interface51528
Node: Configuring your peer52994
Node: Configuring the Friend-to-Friend (F2F) mode53851
Node: Configuring the hostlist to bootstrap55565
Node: Configuration of the HOSTLIST proxy settings58234
Node: Configuring your peer to provide a hostlist59994
Node: Configuring the datastore62227
Node: Configuring the MySQL database63084
Node: Reasons for using MySQL63567
Node: Reasons for not using MySQL64127
Node: Setup Instructions64454
Node: Testing65679
Node: Performance Tuning66315
Node: Setup for running Testcases66970
Node: Configuring the Postgres database67396
Node: Reasons to use Postgres67823
Node: Reasons not to use Postgres68117
Node: Manual setup instructions68414
Node: Testing the setup manually69562
Node: Configuring the datacache70096
Node: Configuring the file-sharing service70812
Node: Configuring logging72542
Node: Configuring the transport service and plugins74737
Node: Configuring the WLAN transport plugin78637
Node: Requirements for the WLAN plugin79454
Node: Configuration79998
Node: Before starting GNUnet80727
Node: Limitations and known bugs81434
Node: Configuring HTTP(S) reverse proxy functionality using Apache or nginx82183
Node: Reverse Proxy - Configure your Apache2 HTTP webserver83881
Node: Reverse Proxy - Configure your Apache2 HTTPS webserver84684
Node: Reverse Proxy - Configure your nginx HTTPS webserver85833
Node: Reverse Proxy - Configure your nginx HTTP webserver87038
Node: Reverse Proxy - Configure your GNUnet peer87975
Node: Blacklisting peers88690
Node: Configuration of the HTTP and HTTPS transport plugins90785
Node: Configuring the GNU Name System92560
Node: Configuring system-wide DNS interception93048
Node: Configuring the GNS nsswitch plugin96481
Node: GNS Proxy Setup98070
Node: Setup of the GNS CA100004
Node: Testing the GNS setup101185
Node: Migrating existing DNS zones into GNS102511
Node: Configuring the GNUnet VPN104847
Node: IPv4 address for interface106064
Node: IPv6 address for interface106932
Node: Configuring the GNUnet VPN DNS107424
Node: Configuring the GNUnet VPN Exit Service107747
Node: IP Address of external DNS resolver109741
Node: IPv4 address for Exit interface110139
Node: IPv6 address for Exit interface110663
Node: Bandwidth Configuration111589
Node: Configuring NAT111966
Node: Peer configuration for distributors (e.g. Operating Systems)115213
Node: Config Leftovers116536
Node: The Single-User Setup118253
Node: The Multi-User Setup120433
Node: Killing GNUnet services122478
Node: Access Control for GNUnet123162
Node: Recommendation - Disable access to services via TCP126186
Node: Recommendation - Run most services as system user "gnunet"127353
Node: Recommendation - Control access to services using group "gnunet"128148
Node: Recommendation - Limit access to certain SUID binaries by group "gnunet"129228
Node: Recommendation - Limit access to critical gnunet-helper-dns to group "gnunetdns"129963
Node: Differences between "make install" and these recommendations131272
Node: Using GNUnet132167
Node: Start and stop GNUnet133305
Node: First steps - Using the GNU Name System133625
Node: Preliminaries134129
Node: Managing Egos134841
Node: The GNS Tab135849
Node: Creating a Record137432
Node: Resolving GNS records139017
Node: Integration with Browsers139823
Node: Creating a Business Card142926
Node: Be Social144947
Node: Backup of Identities and Egos146581
Node: Revocation147524
Node: What's Next?150111
Node: First steps - Using GNUnet Conversation151075
Node: Testing your Audio Equipment151537
Node: GNS Zones152224
Node: Picking an Identity152618
Node: Calling somebody154696
Node: First steps - Using the GNUnet VPN155837
Node: VPN Preliminaries156198
Node: GNUnet-Exit configuration157329
Node: GNS configuration158142
Node: Accessing the service159175
Node: Using a Browser160145
Node: File-sharing160719
Node: fs-Searching162052
Node: fs-Downloading163480
Node: fs-Publishing164802
Node: Important command-line options165309
Node: Indexing vs. Inserting166485
Node: fs-Concepts168542
Node: Files169555
Node: Keywords169983
Node: Directories170797
Node: Egos and File-Sharing171934
Node: Namespaces172987
Node: Advertisements173582
Node: Anonymity level174241
Node: Content Priority175398
Node: Replication176357
Node: Namespace Management176840
Node: Creating Egos177195
Node: Deleting Egos177779
Node: File-Sharing URIs178254
Node: Encoding of hash values in URIs179110
Node: Content Hash Key (chk)179392
Node: Location identifiers (loc)180209
Node: Keyword queries (ksk)180979
Node: Namespace content (sks)181733
Node: GTK User Interface182843
Node: gtk-Publishing183307
Node: gtk-Searching186637
Node: gtk-Downloading187944
Node: The GNU Name System189218
Node: Creating a Zone190927
Node: Maintaining your own Zones191457
Node: Obtaining your Zone Key192894
Node: Adding Links to Other Zones193904
Node: Using Public Keys as Top Level Domains195445
Node: Resource Records in GNS196022
Node: NICK197626
Node: PKEY198536
Node: BOX199062
Node: LEHO199831
Node: VPN200353
Node: A AAAA and TXT201194
Node: CNAME201412
Node: GNS2DNS201852
Node: SOA SRV PTR and MX203267
Node: PLACE205049
Node: PHONE205219
Node: ID ATTR205389
Node: ID TOKEN205574
Node: ID TOKEN METADATA205777
Node: CREDENTIAL206025
Node: POLICY206206
Node: ATTRIBUTE206369
Node: ABE KEY206542
Node: ABE MASTER206711
Node: RECLAIM OIDC CLIENT206900
Node: RECLAIM OIDC REDIRECT207126
Node: Synchronizing with legacy DNS207343
Node: Migrating an existing DNS zone into GNS208917
Node: reclaimID Identity Provider213129
Node: Managing Attributes213976
Node: Sharing Attributes with Third Parties214817
Node: Revoking Authorizations of Third Parties216278
Node: OpenID Connect217059
Node: Using the Virtual Public Network219910
Node: Setting up an Exit node221902
Node: Fedora and the Firewall225003
Node: Setting up VPN node for protocol translation and tunneling226454
Node: GNUnet Contributors Handbook229907
Node: Contributing to GNUnet230253
Node: Licenses of contributions230429
Node: Copyright Assignment231332
Node: Contributing to the Reference Manual232592
Node: Contributing testcases233304
Node: GNUnet Developer Handbook234317
Node: Developer Introduction237129
Node: Project overview239617
Node: Internal dependencies242263
Node: Code overview244922
Node: System Architecture256684
Node: Subsystem stability259564
Node: Naming conventions and coding style guide263909
Node: Naming conventions264269
Node: include files264656
Node: binaries265220
Node: logging265689
Node: configuration266890
Node: exported symbols267310
Node: private (library-internal) symbols (including structs and macros)267677
Node: testcases268339
Node: performance tests268677
Node: src/ directories269061
Node: Coding style269721
Node: Build-system277528
Node: Developing extensions for GNUnet using the gnunet-ext template278547
Node: Writing testcases279878
Node: Building GNUnet and its dependencies282207
Node: TESTING library292678
Node: API294487
Node: Finer control over peer stop297064
Node: Helper functions298570
Node: Testing with multiple processes299536
Node: Performance regression analysis with Gauger302845
Node: TESTBED Subsystem305752
Node: Supported Topologies310290
Node: Hosts file format313849
Node: Topology file format315181
Node: Testbed Barriers315901
Node: Implementation318806
Node: TESTBED Caveats322632
Node: CORE must be started322921
Node: ATS must want the connections323881
Node: libgnunetutil324875
Node: Logging326950
Node: Examples336275
Node: Log files338350
Node: Updated behavior of GNUNET_log340664
Node: Interprocess communication API (IPC)343988
Node: Define new message types345121
Node: Define message struct345627
Node: Client - Establish connection346521
Node: Client - Initialize request message347039
Node: Client - Send request and receive response348441
Node: Server - Startup service349435
Node: Server - Add new handles for specified messages349980
Node: Server - Process request message352045
Node: Server - Response to client354141
Node: Server - Notification of clients355456
Node: Conversion between Network Byte Order (Big Endian) and Host Byte Order356780
Node: Cryptography API359507
Node: Message Queue API361553
Node: Service API367072
Node: Optimizing Memory Consumption of GNUnet's (Multi-) Hash Maps371005
Node: Analysis371970
Node: Solution374196
Node: Migration375197
Node: Conclusion376981
Node: Availability377632
Node: CONTAINER_MDLL API378157
Node: Automatic Restart Manager (ARM)380815
Node: Basic functionality381699
Node: Key configuration options382897
Node: ARM - Availability385269
Node: Reliability387259
Node: TRANSPORT Subsystem389249
Node: Address validation protocol391646
Node: NAT library395217
Node: Distance-Vector plugin397973
Node: SMTP plugin401740
Node: Why use SMTP for a peer-to-peer transport?402865
Node: How does it work?404424
Node: How do I configure my peer?405187
Node: How do I test if it works?407213
Node: How fast is it?408402
Node: Bluetooth plugin412502
Node: What do I need to use the Bluetooth plugin transport?413359
Node: How does it work2?414156
Node: What possible errors should I be aware of?415654
Node: How do I configure my peer2?417603
Node: How can I test it?418732
Node: The implementation of the Bluetooth transport plugin420365
Node: Linux functionality421409
Node: THE INITIALIZATION421943
Node: THE LOOP423409
Node: Details about the broadcast implementation425982
Node: Pending features428383
Node: WLAN plugin428960
Node: ATS Subsystem429270
Node: CORE Subsystem430525
Node: Limitations432317
Node: When is a peer "connected"?434276
Node: libgnunetcore436159
Node: The CORE Client-Service Protocol439640
Node: Setup2440049
Node: Notifications441228
Node: Sending442107
Node: The CORE Peer-to-Peer Protocol443147
Node: Creating the EphemeralKeyMessage443467
Node: Establishing a connection445825
Node: Encryption and Decryption446854
Node: Type maps448461
Node: CADET Subsystem450527
Node: libgnunetcadet452512
Node: NSE Subsystem456882
Node: Motivation457895
Node: Security458634
Node: Principle459303
Node: Example459816
Node: Algorithm460534
Node: Target value460874
Node: Timing461334
Node: Controlled Flooding462235
Node: Calculating the estimate463293
Node: libgnunetnse464305
Node: Results465212
Node: libgnunetnse - Examples466764
Node: The NSE Client-Service Protocol467781
Node: The NSE Peer-to-Peer Protocol468663
Node: HOSTLIST Subsystem471287
Node: HELLOs472731
Node: Overview for the HOSTLIST subsystem473243
Node: Features474458
Node: HOSTLIST - Limitations475025
Node: Interacting with the HOSTLIST daemon475359
Node: Hostlist security address validation476623
Node: The HOSTLIST daemon477709
Node: The HOSTLIST server479171
Node: The HTTP Server479672
Node: Advertising the URL481048
Node: The HOSTLIST client481494
Node: Bootstrapping482147
Node: Learning483484
Node: Usage484368
Node: IDENTITY Subsystem484811
Node: libgnunetidentity487569
Node: Connecting to the service487909
Node: Operations on Egos489756
Node: The anonymous Ego490963
Node: Convenience API to lookup a single ego491609
Node: Associating egos with service functions492356
Node: The IDENTITY Client-Service Protocol492868
Node: NAMESTORE Subsystem494586
Node: libgnunetnamestore496174
Node: Editing Zone Information497074
Node: Iterating Zone Information498990
Node: Monitoring Zone Information499978
Node: PEERINFO Subsystem500877
Node: PEERINFO - Features502163
Node: PEERINFO - Limitations502506
Node: DeveloperPeer Information502748
Node: Startup504486
Node: Managing Information505312
Node: Obtaining Information506376
Node: The PEERINFO Client-Service Protocol507162
Node: libgnunetpeerinfo508760
Node: Connecting to the PEERINFO Service509304
Node: Adding Information to the PEERINFO Service509825
Node: Obtaining Information from the PEERINFO Service510723
Node: PEERSTORE Subsystem511992
Node: Functionality512747
Node: Architecture513600
Node: libgnunetpeerstore514119
Node: SET Subsystem516451
Node: Local Sets517155
Node: Set Modifications517773
Node: Set Operations518375
Node: Result Elements519110
Node: libgnunetset520218
Node: Sets520477
Node: Listeners521128
Node: Operations521691
Node: Supplying a Set522047
Node: The Result Callback522627
Node: The SET Client-Service Protocol523307
Node: Creating Sets523679
Node: Listeners2524125
Node: Initiating Operations524784
Node: Modifying Sets525172
Node: Results and Operation Status525473
Node: Iterating Sets525833
Node: The SET Intersection Peer-to-Peer Protocol526399
Node: The Bloom filter exchange527826
Node: Salt529148
Node: The SET Union Peer-to-Peer Protocol529996
Node: STATISTICS Subsystem532222
Node: libgnunetstatistics534889
Node: Statistics retrieval536210
Node: Setting statistics and updating them537440
Node: Watches538870
Node: The STATISTICS Client-Service Protocol539780
Node: Statistics retrieval2540093
Node: Setting and updating statistics540788
Node: Watching for updates541895
Node: Distributed Hash Table (DHT)542380
Node: Block library and plugins544457
Node: What is a Block?544740
Node: The API of libgnunetblock545519
Node: Queries547193
Node: Sample Code548700
Node: Conclusion2549108
Node: libgnunetdht549715
Node: GET550213
Node: PUT551563
Node: MONITOR552654
Node: DHT Routing Options553685
Node: The DHT Client-Service Protocol555309
Node: PUTting data into the DHT555636
Node: GETting data from the DHT556698
Node: Monitoring the DHT558844
Node: The DHT Peer-to-Peer Protocol559698
Node: Routing GETs or PUTs560004
Node: PUTting data into the DHT2560782
Node: GETting data from the DHT2562002
Node: GNU Name System (GNS)564022
Node: libgnunetgns566451
Node: Looking up records566995
Node: Accessing the records569055
Node: Creating records569692
Node: Future work570148
Node: libgnunetgnsrecord570490
Node: Value handling571577
Node: Type handling572129
Node: GNS plugins572875
Node: The GNS Client-Service Protocol574194
Node: Hijacking the DNS-Traffic using gnunet-service-dns575237
Node: Network Setup Details576941
Node: Importing DNS Zones into GNS577725
Node: Conversions between DNS and GNS578485
Node: DNS Zone Size582603
Node: Performance584403
Node: GNS Namecache586092
Node: libgnunetnamecache587856
Node: The NAMECACHE Client-Service Protocol588974
Node: Lookup589548
Node: Store590113
Node: The NAMECACHE Plugin API590611
Node: Lookup2590992
Node: Store2591400
Node: REVOCATION Subsystem592230
Node: Dissemination592966
Node: Revocation Message Design Requirements594175
Node: libgnunetrevocation595440
Node: Querying for revoked keys595833
Node: Preparing revocations596266
Node: Issuing revocations598113
Node: The REVOCATION Client-Service Protocol598714
Node: The REVOCATION Peer-to-Peer Protocol599800
Node: File-sharing (FS) Subsystem602561
Node: Encoding for Censorship-Resistant Sharing (ECRS)604157
Node: Namespace Advertisements605194
Node: KSBlocks605899
Node: File-sharing persistence directory structure607132
Node: REGEX Subsystem611385
Node: How to run the regex profiler612040
Node: REST Subsystem616106
Node: Namespace considerations617063
Node: Endpoint documentation617696
Node: RPS Subsystem617954
Ref: RPS Subsystem-Footnote-1620022
Node: Brahms620142
Node: GNU Free Documentation License621522
Node: GNU General Public License646906
Node: GNU Affero General Public License684733
Node: Concept Index721952
Node: Programming Index733634

End Tag Table


Local Variables:
coding: utf-8
End:
